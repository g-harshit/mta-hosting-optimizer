package serviceselector

import (
	"errors"
	"sort"

	"gitlab.com/g-harshit/mta-hosting-optimizer/config"
	"gitlab.com/g-harshit/mta-hosting-optimizer/host"
)

func getInValidServices() ([]string, int, error) {
	var resp []string
	data, err := getAllServices()

	cfg, cfgError := config.GetConfig()
	threshold := 1
	if cfgError == nil && cfg.MTA.Threshold > 0 {
		threshold = cfg.MTA.Threshold
	}

	resp = getInvalidHostfromData(data, threshold)

	if err != nil {
		return resp, 404, err
	}

	return resp, 200, nil
}

func getAllServices() (map[string]map[string]bool, error) {
	mtaServiceObj := host.GetService()
	if mtaServiceObj == nil {
		return nil, errors.New("service not found")
	}
	return parsellServiceData(mtaServiceObj.GetAllService()), nil
}

func parsellServiceData(data []host.HostData) map[string]map[string]bool {
	resp := make(map[string]map[string]bool)
	for _, host := range data {
		if _, ok := resp[host.HostName]; ok {
			resp[host.HostName][host.IP] = host.Active
		} else {
			resp[host.HostName] = map[string]bool{host.IP: host.Active}
		}
	}
	return resp
}

func getInvalidHostfromData(data map[string]map[string]bool, threshold int) []string {
	resp := []string{}

	count := 0
	for host, ipdData := range data {
		count = 0
		for _, active := range ipdData {
			if active {
				count++
			}
		}
		if count <= threshold {
			resp = append(resp, host)
		}
	}
	resp = sort.StringSlice(resp)
	return resp
}
