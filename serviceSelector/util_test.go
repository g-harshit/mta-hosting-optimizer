package serviceselector

import (
	"reflect"
	"testing"

	"github.com/golang/mock/gomock"
	"gitlab.com/g-harshit/mta-hosting-optimizer/host"
	mock_host "gitlab.com/g-harshit/mta-hosting-optimizer/host/mockService"
)

func Test_getInValidServices(t *testing.T) {
	tests := []struct {
		Name      string
		WantError error
		WantData  []string
		WantCode  int
		mocFunc   func(service *mock_host.MockService)
	}{
		{
			Name:      "Success",
			WantError: nil,
			WantData:  []string{"mta-prod-1", "mta-prod-3"},
			WantCode:  200,
			mocFunc: func(service *mock_host.MockService) {
				service.EXPECT().GetAllService().Return([]host.HostData{
					{"127.0.0.1", "mta-prod-1", true},
					{"127.0.0.2", "mta-prod-1", false},
					{"127.0.0.3", "mta-prod-2", true},
					{"127.0.0.4", "mta-prod-2", true},
					{"127.0.0.5", "mta-prod-2", false},
					{"127.0.0.6", "mta-prod-3", false},
				})
			},
		},
		{
			Name:      "No resp from service",
			WantError: nil,
			WantData:  []string{},
			WantCode:  200,
			mocFunc: func(service *mock_host.MockService) {
				service.EXPECT().GetAllService().Return([]host.HostData{})
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.Name, func(t *testing.T) {

			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			service := mock_host.NewMockService(ctrl)
			host.Init(service)
			tt.mocFunc(service)

			gotData, gotCode, gotError := getInValidServices()
			if gotError != tt.WantError {
				t.Errorf("getInValidServices() got Error = %v and want Error = %v", gotError, tt.WantError)
			}
			if !reflect.DeepEqual(gotData, tt.WantData) {
				t.Errorf("getInValidServices() got Data = %v and want Data = %v", gotData, tt.WantData)
			}
			if gotCode != tt.WantCode {
				t.Errorf("getInValidServices() got Code = %v and want Code = %v", gotCode, tt.WantCode)
			}
		})
	}
}

func Test_getInvalidHostfromData(t *testing.T) {
	type args struct {
		data      map[string]map[string]bool
		threshold int
	}
	tests := []struct {
		Name string
		Want []string
		Args args
	}{
		{
			Name: "Prod1 : 1 Active, Prod2 : 2 Active, Prod3 0 Active | Thresold 1",
			Args: args{
				data: parsellServiceData([]host.HostData{
					{"127.0.0.1", "mta-prod-1", true},
					{"127.0.0.2", "mta-prod-1", false},
					{"127.0.0.3", "mta-prod-2", true},
					{"127.0.0.4", "mta-prod-2", true},
					{"127.0.0.5", "mta-prod-2", false},
					{"127.0.0.6", "mta-prod-3", false},
				}),
				threshold: 1,
			},
			Want: []string{"mta-prod-1", "mta-prod-3"},
		},
		{
			Name: "Prod1 : 1 Active, Prod2 : 2 Active, Prod3 0 Active | Thresold 2",
			Args: args{
				data: parsellServiceData([]host.HostData{
					{"127.0.0.1", "mta-prod-1", true},
					{"127.0.0.2", "mta-prod-1", false},
					{"127.0.0.3", "mta-prod-2", true},
					{"127.0.0.4", "mta-prod-2", true},
					{"127.0.0.5", "mta-prod-2", false},
					{"127.0.0.6", "mta-prod-3", false},
				}),
				threshold: 2,
			},
			Want: []string{"mta-prod-1", "mta-prod-2", "mta-prod-3"},
		},
		{
			Name: "Prod1 : 1 Active, Prod2 : 2 Active, Prod3 0 Active | Thresold 0",
			Args: args{
				data: parsellServiceData([]host.HostData{
					{"127.0.0.1", "mta-prod-1", true},
					{"127.0.0.2", "mta-prod-1", false},
					{"127.0.0.3", "mta-prod-2", true},
					{"127.0.0.4", "mta-prod-2", true},
					{"127.0.0.5", "mta-prod-2", false},
					{"127.0.0.6", "mta-prod-3", false},
				}),
				threshold: 0,
			},
			Want: []string{"mta-prod-3"},
		},
		{
			Name: "Prod1 : 1 Active, Prod2 : 2 Active, Prod3 1 Active | Thresold 1",
			Args: args{
				data: parsellServiceData([]host.HostData{
					{"127.0.0.1", "mta-prod-1", true},
					{"127.0.0.2", "mta-prod-1", false},
					{"127.0.0.3", "mta-prod-2", true},
					{"127.0.0.4", "mta-prod-2", true},
					{"127.0.0.5", "mta-prod-2", false},
					{"127.0.0.6", "mta-prod-3", true},
				}),
				threshold: 1,
			},
			Want: []string{"mta-prod-1", "mta-prod-3"},
		},
	}

	for _, tt := range tests {
		t.Run(tt.Name, func(t *testing.T) {

			got := getInvalidHostfromData(tt.Args.data, tt.Args.threshold)

			if !reflect.DeepEqual(got, tt.Want) {
				t.Errorf("getInvalidHostfromData() got Data = %v and want Data = %v", got, tt.Want)
			}
		})
	}
}
