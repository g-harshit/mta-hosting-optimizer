package serviceselector

import (
	"github.com/gin-gonic/gin"
)

//LoadServices : Load services
func LoadServices(r *gin.Engine) {
	v1 := r.Group("v1")
	{
		v1.GET("/", GetInValidService)
	}
}

func GetInValidService(c *gin.Context) {
	var resp interface{}
	data, httpCode, err := getInValidServices()
	if err != nil {
		resp = err.Error()
	} else {
		resp = data
	}
	c.JSON(httpCode, resp)
}
