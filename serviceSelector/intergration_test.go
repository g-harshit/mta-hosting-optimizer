package serviceselector

import (
	"reflect"
	"testing"

	"gitlab.com/g-harshit/mta-hosting-optimizer/host"
)

func TestIntegrationGetAllService(t *testing.T) {
	test := []struct {
		Name string
		Want []host.HostData
	}{
		{
			Name: "Success Data Fetch",
			Want: []host.HostData{
				{"127.0.0.1", "mta-prod-1", true},
				{"127.0.0.2", "mta-prod-1", false},
				{"127.0.0.3", "mta-prod-2", true},
				{"127.0.0.4", "mta-prod-2", true},
				{"127.0.0.5", "mta-prod-2", false},
				{"127.0.0.6", "mta-prod-3", false},
			},
		},
	}
	for _, tt := range test {

		mtaServiceObj := host.MtaServiceObj{}
		host.Init(&mtaServiceObj)

		t.Run(tt.Name, func(t *testing.T) {
			if got := host.GetService().GetAllService(); !reflect.DeepEqual(got, tt.Want) {

			}
		})
	}
}
