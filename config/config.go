package config

import "errors"

var Cfg *MainConfig

type MainConfig struct {
	MTA struct {
		Threshold int
	}
}

func GetConfig() (*MainConfig, error) {
	if Cfg == nil {
		return nil, errors.New("no config")
	}
	return Cfg, nil
}
