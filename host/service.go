package host

//go:generate mockgen -destination mockService/mock_service.go github.com/g-harshit/mta-hosting-optimizer/serviceProvider `Service`
type Service interface {
	GetAllService() []HostData
}

var DefaultObject Service

func Init(obj Service) {
	DefaultObject = obj
}

func GetService() Service {
	return DefaultObject
}

func (s *MtaServiceObj) GetAllService() []HostData {
	return []HostData{
		{"127.0.0.1", "mta-prod-1", true},
		{"127.0.0.2", "mta-prod-1", false},
		{"127.0.0.3", "mta-prod-2", true},
		{"127.0.0.4", "mta-prod-2", true},
		{"127.0.0.5", "mta-prod-2", false},
		{"127.0.0.6", "mta-prod-3", false},
	}
}
