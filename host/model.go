package host

type HostData struct {
	IP       string
	HostName string
	Active   bool
}

type MtaServiceObj struct{}
