package main

import (
	"fmt"

	"github.com/gin-gonic/gin"
	"gitlab.com/g-harshit/mta-hosting-optimizer/config"
	"gitlab.com/g-harshit/mta-hosting-optimizer/host"
	"gitlab.com/g-harshit/mta-hosting-optimizer/serviceselector"
	"gopkg.in/gcfg.v1"
)

func main() {

	var mainConfig config.MainConfig
	err := gcfg.ReadFileInto(&mainConfig, "config.ini")
	if err == nil {
		config.Cfg = &mainConfig
	} else {
		fmt.Println(err)
	}

	mtaServiceObj := host.MtaServiceObj{}
	host.Init(&mtaServiceObj)

	r := gin.Default()
	serviceselector.LoadServices(r)
	r.Run(":9092")

}
